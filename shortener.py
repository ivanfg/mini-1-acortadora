import webapp


class shortener (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is initialized
    with the web content."""

    # Declare and initialize content
    urls = {"/": ""}

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, cuerpo

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        metodo,resourceName,cuerpo = parsedRequest

        if metodo == "POST":
            if (cuerpo.split('=')[1][0]) != "&" and cuerpo.split('=')[2] != "":
                claveurl = cuerpo.split("=")[2].replace("%2F", "/")
                self.urls[claveurl] = cuerpo.split("=")[1].split("&")[0].replace('%3A', ':').replace("%2F", '/')

                if (self.urls[claveurl][0:8] != "https://" and self.urls[claveurl][0:7] != "http://"):

                    nuevaurl = "https://" + self.urls[claveurl]
                    self.urls[claveurl] = nuevaurl

                    httpCode = "200 OK"
                    htmlBody = "<html><body>URLS shortened: " + str(self.urls.keys()).split('[')[1].split(']')[0].replace(',', ' ').replace("/", "",1).replace("'","",2).replace("'", "") + \
                               "<p>URLS: " + str(self.urls.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'", "") +\
                               "</p><p><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''>" +\
                               "<p>URL SHORT: <input type = 'text' name = 'nombre' value = ''/>" \
                               "</p></p><p><input type = 'submit' value = 'Enviar' size = '40'></p></form>" \
                               "<br><a href = '" + self.urls[claveurl] + "'>'" + claveurl + "'</a>" \
                               "<br><a href = '" + self.urls[claveurl] + "'>'" + self.urls[claveurl] + "'</a></body></html>"
                else:
                    httpCode = "200 OK"
                    htmlBody = "<html><body>URLS shortened: " + str(self.urls.keys()).split('[')[1].split(']')[0].replace(',', ' ').replace("/", "",1).replace("'","",2).replace("'", "") + \
                               "<p>URLS: " + str(self.urls.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'", "") +\
                               "</p><p><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''>" +\
                               "<p>URL SHORT: <input type = 'text' name = 'nombre' value = ''/>" \
                               "</p></p><p><input type = 'submit' value = 'Enviar' size = '40'></p></form>" \
                               "<br><a href = '" + self.urls[claveurl] + "'>'" + claveurl + "'</a>" \
                               "<br><a href = '" + self.urls[claveurl] + "'>'" + self.urls[claveurl] + "'</a></body></html>"
            else:
                httpCode = "404 ERROR"
                htmlBody = "<html><body><p>Error 404</p></body></html>"

            return httpCode, htmlBody


        if metodo == "GET":
            if resourceName in self.urls.keys() and resourceName != "/":
                httpCode = "301 REDIRECT"
                htmlBody = "<html><body>URLS shortened: " + str(self.urls.keys()).split('[')[1].split(']')[0].replace(',', ' ').replace("/", "",1).replace("'","",2).replace("'", "") +\
                           "<p> URLS: " + str(self.urls.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'", "") +\
                "</p><html><body><p><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''>" \
                "</p></p><p>URL SHORT: <input type = 'text' name = 'nombre' value = ''/></body></html>" \
                "</p><p><input type = 'submit' value = 'Enviar' size = '40'><p>Se te redireccionara a esta pagina en 3,2,1...</p></p></form>" \
                "<meta http-equiv = 'Refresh' content = '3; url=" + self.urls[resourceName] + "'></body></html>"

            elif resourceName == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''" \
                "><p>URL SHORT: <input type = 'text' name = 'nombre' value = ''/>" \
                "</p><p><input type = 'submit' value = 'Enviar' size = '40'></p></form>\</body></html>"

            else:
                httpCode = "404 HTTP ERROR"
                htmlBody = "<html><body><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''" \
                "<p>URL SHORT: <input type = 'text' name = 'nombre' value = ''/>" \
                "</p><p><input type = 'submit' value = 'Enviar' size = '40'></p></form>\</body></html>"
            return httpCode, htmlBody

if __name__ == "__main__":
    testWebApp = shortener("localhost", 1234)

